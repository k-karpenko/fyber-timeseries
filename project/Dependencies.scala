import sbt._
object Dependencies {

  object Versions {
    val scala = "2.11.7"
    val akka = "2.3.12"
    val logback = "1.1.3"
  }

  lazy val server = Def.setting(Seq(
    "com.typesafe.akka" %% "akka-actor" % Versions.akka,
    "com.typesafe.akka" %% "akka-http-experimental" % "1.0",
    "com.typesafe.akka" %% "akka-slf4j" % Versions.akka,
    "com.typesafe.akka" %% "akka-testkit" % Versions.akka % "test",
    "ch.qos.logback" % "logback-core" % Versions.logback,
    "ch.qos.logback" % "logback-classic" % Versions.logback,
    "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2"
  ) )
}
