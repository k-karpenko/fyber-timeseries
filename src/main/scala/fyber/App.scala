package fyber

import java.io.{FileOutputStream, FileInputStream}

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink}
import akka.util.ByteString
import fyber.timeseries.TimeSeries

import scala.concurrent.Await
import scala.concurrent.duration._

import fyber.timeseries.domain._

object App {
  val defaultWindowSize = 60.seconds

  implicit val actorSystem = ActorSystem("fyber-timeseries-processor")
  implicit val actorMateriadlizer = ActorMaterializer()

  /**
   * Use global execution pool to resolve futures
   */
  def main( args: Array[String] ): Unit = {
    import akka.stream.io.Implicits._

    if ( args.length < 1 ) {
      Console.err.println("Path to data file doesn't provided")
      System.exit(1)
    } else if ( args.length < 2 ) {
      Console.err.println("Path to output file doesn't provided")
      System.exit(1)
    } else {
      val output = Sink.outputStream(() => new FileOutputStream(args(1)))
      val source =
        TimeSeries.buildStream(
          inputStream = new FileInputStream(args(0)),
          windowSize = if(args.length > 2) Duration(args(2)) else defaultWindowSize
        )
        .via(
          Flow[TimeFrame].map( frame =>
            ByteString(f"${frame.time}\t${frame.points}\t${frame.sum}%.5f\t${frame.min}%.5f\t${frame.max}%.5f\n")
          )
        )

      Await.ready(source.runWith(output), 10.seconds)

      System.out.println(s"[SUCCESS] - Results path: ${args(1)}")

      actorSystem.shutdown()
    }
  }

}
