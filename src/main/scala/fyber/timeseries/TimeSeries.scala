package fyber.timeseries

import java.io.InputStream

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.io.Framing
import akka.stream.scaladsl.{Flow, Source}
import akka.util.ByteString
import fyber.timeseries.domain._
import fyber.timeseries.stages.StatefulCalculationStage

import scala.concurrent.duration._

object TimeSeries {

  /**
   * By given `java.io.InputStream` (source of operations data) and the `scala.concurrent.duration.Duration` (rolling window size)
   * builds calculation flow and returns subscribable publisher.
   *
   * @param inputStream Source of operations data
   * @param windowSize Rolling window size
   * @return
   */
  def buildStream(inputStream: InputStream,
                  windowSize: Duration)
           (implicit actorSystem: ActorSystem, materializer: ActorMaterializer)
    : Source[TimeFrame, Unit] = {
    import akka.stream.io.Implicits._

    Source.inputStream { () => inputStream }
        .via( Framing.delimiter(delimiter = ByteString("\n"), maximumFrameLength = 512, allowTruncation = false) )
          .map(_.utf8String)
          .map( _.split("\t") )
          .map(p => Operation(p(0).toLong, BigDecimal.valueOf(p(1).toDouble)) )
            .via(Flow[Operation].transform(() => new StatefulCalculationStage(windowSize)))
              .mapMaterializedValue( _ => None )
  }
}
