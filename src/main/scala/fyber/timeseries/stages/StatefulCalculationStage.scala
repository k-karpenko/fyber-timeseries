package fyber.timeseries.stages

import akka.stream.stage.{Context, PushStage, SyncDirective}
import fyber.timeseries.domain.{Operation, TimeFrame}

import scala.concurrent.duration.{Duration, _}

object StatefulCalculationStage {
  private[StatefulCalculationStage] val Zero = BigDecimal.valueOf(0)
}

class StatefulCalculationStage(window: Duration) extends PushStage[Operation, TimeFrame] {
  import StatefulCalculationStage._

  private var initialFrameTime: Long = 0L
  private var previousFrame: Option[TimeFrame] = None

  override def onPush(elem: Operation, ctx: Context[TimeFrame]): SyncDirective = {
    val result = if ( initialFrameTime > 0 ) {
      if ( (elem.ts - initialFrameTime).seconds >= window) {
        initialFrameTime = elem.ts
        buildTimeFrame(None, elem)
      } else buildTimeFrame(previousFrame, elem)
    } else {
      initialFrameTime = elem.ts
      buildTimeFrame(None, elem)
    }

    previousFrame = Some(result)
    ctx.push(result)
  }

  private def buildTimeFrame(previous: Option[TimeFrame], elem: Operation): TimeFrame = {
    TimeFrame(
      time = elem.ts,
      points = previous.map(_.points).getOrElse(0) + 1,
      max = previous.map(_.max).getOrElse(elem.price).max(elem.price),
      min = previous.map(_.min).getOrElse(elem.price).min(elem.price),
      sum = previous.map(_.sum).getOrElse(Zero) + elem.price
    )
  }
}
