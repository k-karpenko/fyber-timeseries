package fyber.timeseries.domain

case class Operation(
    /**
     * number of seconds since epoch
     */
    ts: Long,
    /**
     * value (price ratio)
     */
    price: BigDecimal
)
