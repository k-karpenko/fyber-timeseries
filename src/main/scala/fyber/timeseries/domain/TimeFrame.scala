package fyber.timeseries.domain

case class TimeFrame(
    /**
     * number of seconds since epoch
     */
    time: Long,
     /**
      * Number of operation in the current window
      */
     points: Int,
     /**
      * the maximum in the current window:
      */
     max: BigDecimal,
     /**
      * the minimum in the current window
      */
     min: BigDecimal,
     /**
      * the current rolling sum in the current window
      */
     sum: BigDecimal
)
